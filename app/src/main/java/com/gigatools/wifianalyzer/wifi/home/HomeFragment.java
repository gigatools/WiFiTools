package com.gigatools.wifianalyzer.wifi.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.anastr.speedviewlib.DeluxeSpeedView;
import com.github.anastr.speedviewlib.PointerSpeedometer;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.TimeLineGraphSeries;
import com.gigatools.wifianalyzer.MainContext;
import com.gigatools.wifianalyzer.R;
import com.gigatools.wifianalyzer.settings.Settings;
import com.gigatools.wifianalyzer.widgets.TypefaceManager;
import com.gigatools.wifianalyzer.wifi.accesspoint.ConnectionView;
import com.gigatools.wifianalyzer.wifi.graphutils.GraphConstants;
import com.gigatools.wifianalyzer.wifi.graphutils.GraphViewBuilder;
import com.gigatools.wifianalyzer.wifi.model.WiFiAdditional;
import com.gigatools.wifianalyzer.wifi.model.WiFiConnection;
import com.gigatools.wifianalyzer.wifi.model.WiFiData;
import com.gigatools.wifianalyzer.wifi.model.WiFiDetail;
import com.gigatools.wifianalyzer.wifi.model.WiFiSignal;
import com.gigatools.wifianalyzer.wifi.scanner.UpdateNotifier;
import com.gigatools.wifianalyzer.wifi.timegraph.TimeAxisLabel;

public class HomeFragment extends Fragment implements UpdateNotifier {
    private String TAG = "LUPX";
    private static final int ANIMATION_MOVE_TIME = 300;
    private static final int WIFI_B_SPEED = 11;
    private static final int WIFI_A_SPEED = 54;
    private static final int WIFI_G_SPEED = 72;
    private static final int WIFI_N_SPEED = 150;
    private static final int WIFI_N2_SPEED = 300;
    private static final int WIFI_AC_SPEED = 800;
    private static final int WIFI_AC_80_SPEED = 1800;
    private static final int WIFI_AC_160_SPEED = 4000;

    private PointerSpeedometer signalMeter;
    private DeluxeSpeedView linkSpeedMeter;
    private GraphView signalGraph;
    private GraphView linkSpeedGraph;
    private TimeLineGraphSeries<DataPoint> linkSpeedSeries;
    private TimeLineGraphSeries<DataPoint> signalSeries;
    private int scanCount;
    private int maxLinkSpeed = WIFI_G_SPEED;
    private String networkKey;
    private ConnectionView connection;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainContext.INSTANCE.getScanner().register(this);
    }

    @Override
    public void onDetach() {
        MainContext.INSTANCE.getScanner().unregister(this);
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void addGraphData(WiFiDetail detail) {
        WiFiConnection connection = detail.getWiFiAdditional().getWiFiConnection();


        int level = connection.isConnected() ? detail.getWiFiSignal().getLevel() : -120;
        DataPoint dataPoint = new DataPoint(scanCount, level);
        signalSeries.appendData(dataPoint, true, 100);

        int linkSpeed = connection.isConnected() ? connection.getLinkSpeed() : 0;
        DataPoint linkData = new DataPoint(scanCount, linkSpeed);
        linkSpeedSeries.appendData(linkData, true, 100);
        scanCount++;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_content, container, false);
        signalMeter = (PointerSpeedometer) view.findViewById(R.id.signalMeter);
        signalMeter.setSpeedTextTypeface(TypefaceManager.TYPEFACE_REGULAR);
        signalMeter.setTextTypeface(TypefaceManager.TYPEFACE_REGULAR);

        linkSpeedMeter = view.findViewById(R.id.linkSpeedMeter);
        linkSpeedMeter.setSpeedTextTypeface(TypefaceManager.TYPEFACE_REGULAR);
        linkSpeedMeter.setTextTypeface(TypefaceManager.TYPEFACE_REGULAR);
        connection = new ConnectionView(view);
        setupViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainContext.INSTANCE.getScanner().register(connection);
    }

    @Override
    public void onPause() {
        super.onPause();
        MainContext.INSTANCE.getScanner().unregister(connection);
    }

    private void setupViews(View view) {
        scanCount = 1;
        setupSignalGraph(view);
        setupSpeedGraph(view);
    }

    private void setupSignalGraph(View view) {
        //signal graph
        signalGraph = makeGraphView();
        RelativeLayout signalGraphParentView = view.findViewById(R.id.graphSignal);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        signalGraph.setLayoutParams(params);
        signalGraph.getViewport().setMinX(0);
        signalGraph.getViewport().setMaxX(signalGraph.getGridLabelRenderer().getNumHorizontalLabels() - 1);
        signalGraph.setVisibility(View.VISIBLE); //Because it was GONE in builder
        signalGraph.getGridLabelRenderer().setVerticalAxisTitle(getString(R.string.home_signal_graph_y_title));
        signalGraphParentView.addView(signalGraph);
        signalSeries = new TimeLineGraphSeries<>();
        signalSeries.setAnimated(true);
        signalSeries.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.connected));
        signalGraph.removeAllSeries();
        signalGraph.addSeries(signalSeries);

    }

    private void setupSpeedGraph(View view) {
        //link speed graph
        linkSpeedGraph = makeGraphView();
        RelativeLayout speedGraphLayout = view.findViewById(R.id.speedGraph);
        RelativeLayout.LayoutParams speedParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        linkSpeedGraph.setLayoutParams(speedParams);
        linkSpeedGraph.setVisibility(View.VISIBLE);//Because it was GONE in builder
        speedGraphLayout.addView(linkSpeedGraph);

        GridLabelRenderer labelRenderer = linkSpeedGraph.getGridLabelRenderer();
        labelRenderer.setVerticalAxisTitle(getString(R.string.home_link_speed_graph_y_title));
        labelRenderer.setNumVerticalLabels(11);

        linkSpeedSeries = new TimeLineGraphSeries<>();
        linkSpeedSeries.setAnimated(true);
        linkSpeedSeries.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.connected));
        linkSpeedGraph.removeAllSeries();
        linkSpeedGraph.addSeries(linkSpeedSeries);

        Viewport viewport = linkSpeedGraph.getViewport();
        viewport.setMinY(0);
        viewport.setMaxY(WIFI_G_SPEED);
        viewport.setMinX(0);
        viewport.setMaxX(linkSpeedGraph.getGridLabelRenderer().getNumHorizontalLabels() - 1);
    }

    private GraphView makeGraphView() {
        Settings settings = MainContext.INSTANCE.getSettings();
        return new GraphViewBuilder(MainContext.INSTANCE.getContext(), GraphConstants.NUM_X_TIME, settings.getGraphMaximumY(), settings.getThemeStyle())
                .setLabelFormatter(new TimeAxisLabel())
                .setHorizontalLabelsVisible(true)
                .build();
    }

    private void updateSignalMeter(@NonNull WiFiSignal signal) {
        if (signalMeter != null) {
            signalMeter.speedTo(signal.getLevel(), ANIMATION_MOVE_TIME);
            int color = ContextCompat.getColor(getActivity(), signal.getStrength().colorResource());
            signalMeter.setSpeedometerColor(color);
        }
    }

    private void updateLinkSpeedMeter(@NonNull WiFiAdditional additional) {
        if (linkSpeedMeter != null) {
            int linkSpeed = additional.getWiFiConnection().getLinkSpeed();
            if (linkSpeed < WIFI_B_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_B_SPEED);
            } else if (linkSpeed < WIFI_A_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_A_SPEED);
            } else if (linkSpeed < WIFI_G_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_G_SPEED);
            } else if (linkSpeed < WIFI_N_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_N_SPEED);
                maxLinkSpeed = WIFI_N_SPEED;
            } else if (linkSpeed < WIFI_N2_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_N2_SPEED);
                maxLinkSpeed = WIFI_N2_SPEED;
            } else if (linkSpeed < WIFI_AC_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_AC_SPEED);
                maxLinkSpeed = WIFI_AC_SPEED;
            } else if (linkSpeed < WIFI_AC_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_AC_80_SPEED);
                maxLinkSpeed = WIFI_AC_SPEED;
            } else if (linkSpeed < WIFI_AC_160_SPEED) {
                linkSpeedMeter.setMaxSpeed(WIFI_AC_160_SPEED);
                maxLinkSpeed = WIFI_AC_160_SPEED;
            }
            if (linkSpeedGraph.getViewport().getMaxY(false) < maxLinkSpeed) {
                linkSpeedGraph.getViewport().setMaxY(maxLinkSpeed);
            }
            linkSpeedMeter.speedTo(linkSpeed, ANIMATION_MOVE_TIME);
        }
    }

    private void onDisconnected() {
        linkSpeedMeter.speedTo(0, ANIMATION_MOVE_TIME);
        signalMeter.setMinSpeed(-120);
        signalMeter.setSpeedometerColor(ContextCompat.getColor(getActivity(), R.color.error_color));
        signalMeter.speedTo(-120, ANIMATION_MOVE_TIME);
    }

    @Override
    public void update(@NonNull WiFiData wiFiData) {
        WiFiDetail detail = wiFiData.getConnection();
        if (!detail.getWiFiAdditional().getWiFiConnection().isConnected()) {
            onDisconnected();
            return;
        }
        if (!detail.getBSSID().equals(networkKey)) {
            networkKey = detail.getBSSID();
            if (signalSeries != null) {
                signalSeries.resetData(new DataPoint[]{});
                scanCount = 1;
            }
            if (linkSpeedSeries != null) {
                linkSpeedSeries.resetData(new DataPoint[]{});
            }
        }
        signalMeter.setMinSpeed(-100);
        WiFiSignal signal = detail.getWiFiSignal();
        updateSignalMeter(signal);
        WiFiAdditional additional = detail.getWiFiAdditional();
        updateLinkSpeedMeter(additional);
        addGraphData(detail);
    }
}
