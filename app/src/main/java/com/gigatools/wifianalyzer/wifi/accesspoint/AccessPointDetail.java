/*
 * WiFiAnalyzer
 * Copyright (C) 2017  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.gigatools.wifianalyzer.wifi.accesspoint;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gigatools.wifianalyzer.MainActivity;
import com.gigatools.wifianalyzer.MainContext;
import com.gigatools.wifianalyzer.R;
import com.gigatools.wifianalyzer.wifi.model.Security;
import com.gigatools.wifianalyzer.wifi.model.Strength;
import com.gigatools.wifianalyzer.wifi.model.WiFiAdditional;
import com.gigatools.wifianalyzer.wifi.model.WiFiConnection;
import com.gigatools.wifianalyzer.wifi.model.WiFiDetail;
import com.gigatools.wifianalyzer.wifi.model.WiFiSignal;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public class AccessPointDetail {
    private static final int VENDOR_SHORT_MAX = 12;
    private static final int VENDOR_LONG_MAX = 30;

    View makeView(View convertView, ViewGroup parent, @NonNull WiFiDetail wiFiDetail,
                  boolean isChild) {
        AccessPointViewType accessPointViewType =
                MainContext.INSTANCE.getSettings().getAccessPointView();
        return makeView(convertView, parent, wiFiDetail, isChild, accessPointViewType);
    }

    View makeView(View convertView, ViewGroup parent, @NonNull WiFiDetail wiFiDetail,
                  boolean isChild, @NonNull AccessPointViewType accessPointViewType) {
        MainActivity mainActivity = MainContext.INSTANCE.getMainActivity();
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater layoutInflater = MainContext.INSTANCE.getLayoutInflater();
            view = layoutInflater.inflate(accessPointViewType.getLayout(), parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        setViewCompact(mainActivity, holder, wiFiDetail, isChild);
        setViewExtra(mainActivity, holder, wiFiDetail);
        if (view.findViewById(R.id.vendorShort) != null) {
            setViewVendorShort(holder, wiFiDetail.getWiFiAdditional());
        }
        return view;
    }

    public View makeViewPopup(@NonNull WiFiDetail wiFiDetail) {
        MainActivity mainActivity = MainContext.INSTANCE.getMainActivity();
        View view =
                mainActivity.getLayoutInflater().inflate(R.layout.access_point_view_popup, null);
        ViewHolder holder = new ViewHolder(view);
        setViewCompact(mainActivity, holder, wiFiDetail, false);
        setViewExtra(mainActivity, holder, wiFiDetail);
        setViewVendorLong(view, wiFiDetail.getWiFiAdditional());
        setViewConnection(mainActivity, holder, wiFiDetail.getWiFiAdditional().getWiFiConnection());
        return view;
    }

    private void setViewConnection(@NonNull Context context, @NonNull ViewHolder holder,
                                   @NonNull WiFiConnection connection) {
        if (holder.linkSpeed != null) {
            holder.linkSpeed.setText(connection.isConnected() ? String.valueOf(connection
                    .getLinkSpeed()) : "0");
        }
        if (holder.ipAddress != null) {
            holder.ipAddress
                    .setText(connection.isConnected() ? connection.getIpAddress() : "Disconnected");
        }
    }

    private void setViewCompact(@NonNull Context context, @NonNull ViewHolder holder,
                                @NonNull WiFiDetail wiFiDetail, boolean isChild) {
        holder.ssid.setText(wiFiDetail.getSSID());
        if (holder.bssid != null) {
            holder.bssid.setText(wiFiDetail.getBSSID());
        }
        WiFiSignal wiFiSignal = wiFiDetail.getWiFiSignal();
        Strength strength = wiFiSignal.getStrength();
        Security security = wiFiDetail.getSecurity();
        if (holder.securityImage != null) {
            holder.securityImage.setImageResource(security.getImageResource());
            holder.securityImage
                    .setColorFilter(ContextCompat.getColor(context, R.color.icons_color));
        }
        if (holder.secureWifiIcon != null) {
            holder.secureWifiIcon.setImageResource(security.equals(Security.NONE) ? R.drawable.ic_unlocked : R.drawable.ic_lock);
            holder.secureWifiIcon.setColorFilter(security.equals(Security.NONE) ?
                    ContextCompat.getColor(context, R.color.warning_color) : ContextCompat.getColor(context, R.color.connected));
        }
        holder.signalLevel.setText(String.format(Locale.ENGLISH, "%d", wiFiSignal.getLevel()));
        holder.channel.setText(wiFiSignal.getPrimaryChannelDisplay());
        holder.frequency.setText(String.valueOf(wiFiSignal.getPrimaryFrequency()));
        if (holder.frequencyUnit != null) {
            holder.frequencyUnit.setText(WiFiSignal.FREQUENCY_UNITS);
        }
        holder.distance.setText(String.format(Locale.ENGLISH, "%5.1f", wiFiSignal.getDistance()));
        if (holder.tab != null) {
            holder.tab.setVisibility(isChild ? View.VISIBLE : View.GONE);
        }
    }

    private void setViewExtra(@NonNull Context context, @NonNull final ViewHolder holder, @NonNull WiFiDetail wiFiDetail) {
        if (holder.configurated != null) {
            WiFiAdditional wiFiAdditional = wiFiDetail.getWiFiAdditional();
            if (wiFiAdditional.isConfiguredNetwork()) {
                holder.configurated.setVisibility(View.VISIBLE);
                holder.configurated
                        .setColorFilter(ContextCompat.getColor(context, R.color.connected));
            } else {
                holder.configurated.setVisibility(View.GONE);
            }
        }
        WiFiSignal wiFiSignal = wiFiDetail.getWiFiSignal();
        Strength strength = wiFiSignal.getStrength();
        if (holder.signalColor != null) {
            holder.signalColor.setColorFilter(ContextCompat.getColor(context, strength.colorResource()));
        }
        if (holder.capabilities != null) {
            holder.capabilities.setText(wiFiDetail.getCapabilities());
        }
        if (holder.wifiBand != null) {
            holder.wifiBand.setText(wiFiSignal.getWiFiBand().getTextResource());
        }
        if (holder.wifiBandIcon != null) {
            holder.wifiBandIcon.setText(wiFiSignal.getWiFiBand().isGHZ5() ? "5G" : "2.4G");
        }
        if (holder.channelWidth != null) {
            holder.channelWidth.setText(String.valueOf(wiFiSignal.getWiFiWidth()
                    .getFrequencyWidth()));
        }
    }

    private void setViewVendorShort(@NonNull ViewHolder holder, @NonNull WiFiAdditional wiFiAdditional) {
        String vendor = wiFiAdditional.getVendorName();
        if (StringUtils.isBlank(vendor)) {
            holder.vendor.setVisibility(View.GONE);
        } else {
            holder.vendor.setVisibility(View.VISIBLE);
            holder.vendor.setText(vendor.substring(0, Math.min(VENDOR_SHORT_MAX, vendor.length())));
        }
    }

    private void setViewVendorLong(@NonNull View view, @NonNull WiFiAdditional wiFiAdditional) {
        TextView textVendor = view.findViewById(R.id.vendorLong);
        String vendor = wiFiAdditional.getVendorName();
        if (StringUtils.isBlank(vendor)) {
            textVendor.setVisibility(View.GONE);
        } else {
            textVendor.setVisibility(View.VISIBLE);
            textVendor.setText(vendor.substring(0, Math.min(VENDOR_LONG_MAX, vendor.length())));
        }
    }

    static class ViewHolder {
        private ImageView configurated;
        private TextView ssid;
        private TextView bssid;
        private TextView signalLevel;
        private TextView channel;
        private ImageView signalColor;
        private TextView vendor;
        private TextView frequency;
        private TextView frequencyUnit;
        private TextView distance;
        private TextView tab;
        private ImageView securityImage;
        private TextView capabilities;
        private TextView linkSpeed;
        private TextView ipAddress;
        private TextView wifiBand;
        private TextView wifiBandIcon;
        private TextView channelWidth;
        private ImageView secureWifiIcon;

        public ViewHolder(View view) {
            configurated = (ImageView) view.findViewById(R.id.configuredImage);
            ssid = (TextView) view.findViewById(R.id.ssid);
            bssid = (TextView) view.findViewById(R.id.bssid);
            signalLevel = (TextView) view.findViewById(R.id.level);
            channel = (TextView) view.findViewById(R.id.channel);
            signalColor = (ImageView) view.findViewById(R.id.signalColor);
            vendor = (TextView) view.findViewById(R.id.vendorShort);
            frequency = (TextView) view.findViewById(R.id.primaryFrequency);
            frequencyUnit = (TextView) view.findViewById(R.id.frequencyUnit);
            distance = (TextView) view.findViewById(R.id.distance);
            tab = (TextView) view.findViewById(R.id.tab);
            securityImage = (ImageView) view.findViewById(R.id.securityImage);
            capabilities = (TextView) view.findViewById(R.id.capabilities);
            linkSpeed = (TextView) view.findViewById(R.id.linkSpeed);
            ipAddress = (TextView) view.findViewById(R.id.ipAddress);
            wifiBand = (TextView) view.findViewById(R.id.wifiBand);
            wifiBandIcon = (TextView) view.findViewById(R.id.wifiBandIcon);
            channelWidth = (TextView) view.findViewById(R.id.frequencyWidth);
            secureWifiIcon = (ImageView) view.findViewById(R.id.secureWifiIcon);
        }
    }
}
