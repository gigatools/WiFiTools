package com.gigatools.wifianalyzer.settings;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.view.View;

import com.gigatools.wifianalyzer.widgets.TypefaceManager;

public class TypefaceListPreference extends ListPreference {
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TypefaceListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TypefaceListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TypefaceListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TypefaceListPreference(Context context) {
        super(context);
    }

    @Override
    protected void onBindView(View view) {
        TypefaceManager.applyTypeface(view, TypefaceManager.TYPEFACE_REGULAR);
        super.onBindView(view);
    }
}
