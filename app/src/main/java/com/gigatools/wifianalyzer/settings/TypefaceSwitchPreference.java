package com.gigatools.wifianalyzer.settings;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.preference.SwitchPreference;
import android.util.AttributeSet;
import android.view.View;

import com.gigatools.wifianalyzer.widgets.TypefaceManager;

public class TypefaceSwitchPreference extends SwitchPreference {

    public TypefaceSwitchPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TypefaceSwitchPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public TypefaceSwitchPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TypefaceSwitchPreference(Context context) {
        super(context);
    }

    @Override
    protected void onBindView(View view) {
        TypefaceManager.applyTypeface(view, TypefaceManager.TYPEFACE_REGULAR);
        super.onBindView(view);
    }
}
