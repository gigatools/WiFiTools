/*
 * Copyright 2016 Brightec Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gigatools.wifianalyzer.rateapp;

import android.content.Context;
import android.support.annotation.Nullable;

import com.gigatools.wifianalyzer.BuildConfig;

public class InstanceSettings {
    private static final String PREF_SHOW_SUFFIX = "_show";
    private static final String PREF_RATING_SUFFIX = "_rating";
    private final String PREF_KEY_COUNT = "_count";
    private Context mContext;
    private String mInstanceName;
    private RetryPolicy policy = RetryPolicy.INCREMENTAL;
    private int initialLaunchCount = 2;

    private InstanceSettings(Context context, String instanceName) {
        mContext = context;
        mInstanceName = instanceName;
        if (BuildConfig.DEBUG) {
            initialLaunchCount = 1;
        }
    }

    public void retryPolicy(RetryPolicy policy) {
        this.policy = policy;
    }

    /**
     * Returns the InstanceSettings for the RateTheApp widget
     * Note: If you are using the 'app:rateTheAppName' you need to provide the rateTheAppName you
     * want the InstanceSettings of.
     *
     * @param context Context
     * @return InstanceSettings
     */
    @SuppressWarnings({"unused"})
    public static InstanceSettings getInstanceSettings(Context context) {
        return getInstanceSettings(context, null);
    }

    /**
     * Returns the InstanceSettings for the RateTheApp widget
     * Note: If you are using the 'app:rateTheAppName' you need to provide the rateTheAppName you
     * want the InstanceSettings of.
     *
     * @param context        Context
     * @param rateTheAppName String
     * @return InstanceSettings
     */
    @SuppressWarnings({"unused"})
    public static InstanceSettings getInstanceSettings(Context context, @Nullable String
        rateTheAppName) {
        String instanceName = Utils.getInstanceNameFromRateTheAppName(rateTheAppName);
        return new InstanceSettings(context, instanceName);
    }

    /**
     * Returns a boolean to indicate whether the given instance of RateTheApp should be shown
     *
     * @return boolean TRUE if should be shown
     */
    public boolean shouldShow() {
        boolean isShouldShow =
            Utils.readSharedSetting(mContext, mInstanceName + PREF_SHOW_SUFFIX, true);
        int prevCount = Utils.readSharedSetting(mContext, mInstanceName + PREF_KEY_COUNT, 0);
        if (prevCount == 0) {
            return false;
        }
        if (prevCount == initialLaunchCount) {
            return isShouldShow;
        } else if (policy == RetryPolicy.INCREMENTAL && prevCount % initialLaunchCount == 0) {
            return isShouldShow;
        } else if (policy == RetryPolicy.EXPONENTIAL && prevCount % initialLaunchCount == 0 &&
            isPowerOfTwo(prevCount / initialLaunchCount)) {
            return isShouldShow;
        }
        return false;
    }

    public static boolean isPowerOfTwo(int x) {
        return (x & (x - 1)) == 0;
    }

    /**
     * Set the given instance of RateTheApp to hide permanently
     */
    public void hidePermanently() {
        Utils.saveSharedSetting(mContext, mInstanceName + PREF_SHOW_SUFFIX, false);
    }

    public void incrementViews() {
        String key = mInstanceName + PREF_KEY_COUNT;
        int prevCount = Utils.readSharedSetting(mContext, key, 0);
        Utils.saveSharedSetting(mContext, key, prevCount + 1);
    }

    public void reset() {
        Utils.saveSharedSetting(mContext, mInstanceName + PREF_SHOW_SUFFIX, true);
        Utils.saveSharedSetting(mContext, mInstanceName + PREF_KEY_COUNT, 0);
        Utils.saveSharedSetting(mContext, mInstanceName + PREF_RATING_SUFFIX, 0);
    }

    /**
     * Save the rating on the given instance of RateTheApp
     *
     * @param rating float
     */
    public void saveRating(float rating) {
        Utils.saveSharedSetting(mContext, mInstanceName + PREF_RATING_SUFFIX, rating);
    }

    /**
     * Get the rating on the given instance of RateTheApp
     *
     * @param defaultRating float The default value you would like returned if no rating has been
     *                      saved
     * @return float
     */
    public float getSavedRating(float defaultRating) {
        return Utils.readSharedSetting(mContext, mInstanceName + PREF_RATING_SUFFIX, defaultRating);
    }

    /**
     * Reset the given instance of RateTheApp widget's rating and visibility.
     */
    public void resetWidget() {
        // Reset the widget visibility to true
        Utils.saveSharedSetting(mContext, mInstanceName + PREF_SHOW_SUFFIX, true);
        // Rest the widget rating to zero
        saveRating(0f);
    }

    public enum RetryPolicy {
        /**
         * Will retry each time initial count has been triggered
         * Ex: if initial is set to 3, it will be shown on the 3rd, 6th, 9th, ... times
         */
        INCREMENTAL,
        /**
         * Will retry exponentially to be less intrusive
         * Ex: if initial is set to 3, it will be shown on the 3rd, 6th, 12th, ... times
         */
        EXPONENTIAL,
        /**
         * Will never retry
         */
        NONE
    }
}
