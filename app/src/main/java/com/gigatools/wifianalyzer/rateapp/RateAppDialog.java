package com.gigatools.wifianalyzer.rateapp;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gigatools.wifianalyzer.R;

public class RateAppDialog extends DialogFragment implements View.OnClickListener {

    private RateAppView rateAppView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.dialog_rate_this_app, container, false);
        rateAppView = (RateAppView) view.findViewById(R.id.rate_the_app);
        rateAppView.setOnCloseClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        rateAppView.setContainer(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
