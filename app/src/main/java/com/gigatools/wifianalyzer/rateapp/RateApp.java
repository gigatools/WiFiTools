package com.gigatools.wifianalyzer.rateapp;

import android.app.Activity;
import android.view.ViewGroup;

import com.gigatools.wifianalyzer.BuildConfig;

public class RateApp {
    private ViewGroup layout;
    private Activity context;
    private InstanceSettings instanceSettings;

    private RateApp(Activity context) {
        this.context = context;
        this.layout = layout;
        instanceSettings = InstanceSettings.getInstanceSettings(context, BuildConfig
                .APPLICATION_ID);
    }

    public static RateApp with(Activity context) {
        RateApp instance = new RateApp(context);
        return instance;
    }

    public void checkAndShow() {
        if (!instanceSettings.shouldShow()) {
            instanceSettings.incrementViews();
            return;
        }
        RateAppDialog dialog = new RateAppDialog();
        dialog.show(context.getFragmentManager(), "Rate App");
    }
}
