package com.gigatools.wifianalyzer.rateapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gigatools.wifianalyzer.BuildConfig;
import com.gigatools.wifianalyzer.R;
import com.gigatools.wifianalyzer.rateapp.materialratingbar.MaterialRatingBar;

public class RateAppView extends LinearLayout {
    static final String INSTANCE_PREFIX = "ratetheapp";
    private static final int DEFAULT_NUMBER_OF_STARS = 5;
    private static final float DEFAULT_STEP_SIZE = 1f;
    private static final float DEFAULT_RATING = 0.0f;
    private String mTitleStr, mMessageStr;
    private MaterialRatingBar mRatingBar;
    private int mNumberOfStars = DEFAULT_NUMBER_OF_STARS;
    private float mStepSize = DEFAULT_STEP_SIZE;
    private float mDefaultRating = DEFAULT_RATING;
    private boolean mSaveRating;
    private InstanceSettings mInstanceSettings;
    private TextView mTextTitle, mTextMessage;
    private ImageView mActionClose;
    private ViewGroup container;
    private OnClickListener onCloseClickListener;
    private OnUserSelectedRatingListener mOnUserSelectedRatingListener;
    private MaterialRatingBar.OnRatingChangeListener ratingChangeListener =
            new MaterialRatingBar.OnRatingChangeListener() {
                @Override
                public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                    if (onCloseClickListener != null) {
                        onCloseClickListener.onClick(ratingBar);
                    }
                    // Save the rating
                    if (mSaveRating) {
                        mInstanceSettings.saveRating(rating);
                    }
                    // If a rateChangeListener was provided, call it
                    if (mOnUserSelectedRatingListener != null) {
                        mOnUserSelectedRatingListener.onRatingChanged(RateAppView.this, rating);
                    }
                }
            };

    public RateAppView(Context context) {
        super(context);
        init();
    }

    public void setOnCloseClickListener(OnClickListener onCloseClickListener) {
        this.onCloseClickListener = onCloseClickListener;
    }

    public void setInstanceSettings(InstanceSettings instanceSettings) {
        this.mInstanceSettings = instanceSettings;
    }

    public RateAppView(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadAttributes(attrs);
        init();
    }

    public RateAppView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        loadAttributes(attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RateAppView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        loadAttributes(attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void loadAttributes(AttributeSet attrs) {
        loadAttributes(attrs, 0, 0);
    }

    private void loadAttributes(AttributeSet attrs, int defStyleAttr) {
        loadAttributes(attrs, defStyleAttr, 0);
    }

    private void loadAttributes(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.RateAppView, defStyleAttr, defStyleRes);
        mTitleStr = a.getString(R.styleable.RateAppView_rateTheAppTitleText);
        mMessageStr = a.getString(R.styleable.RateAppView_rateTheAppMessageText);
        // Stars & Rating
        mNumberOfStars =
                a.getInt(R.styleable.RateAppView_rateTheAppNumberOfStars, DEFAULT_NUMBER_OF_STARS);
        mStepSize = a.getFloat(R.styleable.RateAppView_rateTheAppStepSize, DEFAULT_STEP_SIZE);
        mDefaultRating =
                a.getFloat(R.styleable.RateAppView_rateTheAppDefaultRating, DEFAULT_RATING);
        mSaveRating = a.getBoolean(R.styleable.RateAppView_rateTheAppSaveRating, true);
        a.recycle();
    }

    private void init() {
        mInstanceSettings = InstanceSettings.getInstanceSettings(getContext(), BuildConfig
                .APPLICATION_ID);
        if (!isInEditMode() && !shouldShow()) {
            this.setVisibility(GONE);
            return;
        }
        //Inflate and find all the relevant views
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View rootView = inflater.inflate(R.layout.widget_app_rate_stars, this, false);
        mRatingBar = (MaterialRatingBar) rootView.findViewById(R.id.rating_bar);
        mTextTitle = (TextView) rootView.findViewById(R.id.text_rating_title);
        mTextMessage = (TextView) rootView.findViewById(R.id.text_rating_message);
        mActionClose = (ImageView) rootView.findViewById(R.id.action_close);
        mActionClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mInstanceSettings.incrementViews();
                hideViews();
                if (onCloseClickListener != null) {
                    onCloseClickListener.onClick(v);
                }
            }
        });
        mRatingBar.setNumStars(mNumberOfStars);
        mRatingBar.setStepSize(mStepSize);
        // Initialise the title
        initTitle();
        // Initialise the message
        initMessage();
        float rating;
        if (isInEditMode()) {
            //Show some rating for editor
            rating = mDefaultRating;
        } else {
            // Set previously saved rating (else use default rating)
            rating = mInstanceSettings.getSavedRating(-1f);
            if (rating == -1f) {
                rating = mDefaultRating;
            }
        }
        if (mSaveRating) {
            mRatingBar.setRating(rating);
        }
        mRatingBar.setOnRatingChangeListener(ratingChangeListener);
        if (!isInEditMode()) {
            // Set the default RateChangeListener
            setOnUserSelectedRatingListener(DefaultOnUserSelectedRatingListener
                    .createDefaultInstance(getContext()));
        }
        addView(rootView);
    }

    private void initTitle() {
        // Hide the title if an empty title text attribute was provided
        if (mTitleStr != null && mTitleStr.isEmpty()) {
            mTextTitle.setVisibility(GONE);
        } else {
            // Set the title text if provided
            if (mTitleStr != null) {
                mTextTitle.setText(mTitleStr);
            }
        }
    }

    private void initMessage() {
        // Hide the message if an empty message text attribute was provided
        if (mMessageStr != null && mMessageStr.isEmpty()) {
            mTextMessage.setVisibility(GONE);
        } else {
            // Set the title text if provided
            if (mMessageStr != null) {
                mTextMessage.setText(mMessageStr);
            }
        }
    }

    /**
     * Retrieve the currently set OnUserSelectedRatingListener
     *
     * @return OnUserSelectedRatingListener
     */
    @SuppressWarnings({"unused"})
    public OnUserSelectedRatingListener getOnUserSelectedRatingListener() {
        return mOnUserSelectedRatingListener;
    }

    /**
     * Set the listener for when the user selects a rating
     * Note: This will not be called if you manually set the rating
     *
     * @param onUserSelectedRatingListener OnUserSelectedRatingListener
     */
    @SuppressWarnings({"unused"})
    public void setOnUserSelectedRatingListener(
            OnUserSelectedRatingListener onUserSelectedRatingListener) {
        mOnUserSelectedRatingListener = onUserSelectedRatingListener;
    }

    /**
     * Returns a boolean to indicate whether RateTheApp should be shown
     *
     * @return boolean TRUE if should be shown
     */
    @SuppressWarnings({"unused"})
    public boolean shouldShow() {
        return mInstanceSettings.shouldShow();
    }

    /**
     * Set the given instance of RateTheApp to hide permanently
     */
    @SuppressWarnings({"unused"})
    public void hidePermanently() {
        hideViews();
        mInstanceSettings.hidePermanently();
    }

    private void hideViews() {
        Animation hideAnimation =
                AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out);
        hideAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (container != null) container.setVisibility(View.GONE);
                else setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        if (container != null) {
            container.startAnimation(hideAnimation);
        } else {
            startAnimation(hideAnimation);
        }
    }

    /**
     * Get the rating currently set on the RateTheApp widget
     *
     * @return float
     */
    @SuppressWarnings({"unused"})
    public float getRating() {
        return mRatingBar.getRating();
    }

    /**
     * Set the rating on RateTheApp widget
     * Note: this will not call OnUserSelectedRatingListener
     *
     * @param rating float
     */
    @SuppressWarnings({"unused"})
    public void setRating(float rating) {
        mRatingBar.setRating(rating);
    }

    /**
     * Reset the given instance of RateTheApp widget's rating and visibility.
     */
    @SuppressWarnings({"unused"})
    public void resetWidget() {
        mInstanceSettings.resetWidget();
    }

    /**
     * This method returns the TextView which represents the title in the layout. This method is provided for specific use cases where this library has not provided the exact config needed.
     *
     * @return TextView mTextTitle - The TextView associated with the title
     */
    @SuppressWarnings({"unused"})
    public TextView getTitleTextView() {
        return mTextTitle;
    }

    /**
     * This method returns the TextView which represents the message in the layout. This method is provided for specific use cases where this library has not provided the exact config needed.
     *
     * @return TextView mTextMessage - The TextView associated with the message
     */
    @SuppressWarnings({"unused"})
    public TextView getMessageTextView() {
        return mTextMessage;
    }

    /**
     * Get the settings for this instance of the RateTheApp widget
     *
     * @return InstanceSettings
     */
    @SuppressWarnings({"unused"})
    public InstanceSettings getInstanceSettings() {
        return mInstanceSettings;
    }

    public interface OnUserSelectedRatingListener {
        void onRatingChanged(RateAppView rateTheApp, float rating);
    }

    public ViewGroup getContainer() {
        return container;
    }

    public void setContainer(ViewGroup container) {
        this.container = container;
    }
}