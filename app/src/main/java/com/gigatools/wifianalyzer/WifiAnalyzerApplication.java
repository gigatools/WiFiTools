package com.gigatools.wifianalyzer;

import android.app.Application;

import com.gigatools.wifianalyzer.ads.GAdsCenter;
import com.gigatools.wifianalyzer.widgets.TypefaceManager;
import com.google.android.gms.ads.MobileAds;

public class WifiAnalyzerApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceManager.initialize(this);
        MobileAds.initialize(this, Configs.ADMOB_APP_ID);
        GAdsCenter
                .getInstance()
                .addAdmobTestDevice("8214B2F56AE20C8CE22CCF9AD69151AB")
                .addFANTestDevice("62e01f775145056a6208b2d1798dc718");
    }
}
