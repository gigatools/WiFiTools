package com.gigatools.wifianalyzer.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TypefaceManager {
    private static final String ASSET_PATH_LIGHT = "font/Roboto-Thin.ttf";
    private static final String ASSET_PATH_REGUALR = "font/Roboto-Light.ttf";
    private static final String ASSET_PATH_BOLD = "font/Roboto-Medium.ttf";
    public static Typeface TYPEFACE_LIGHT;
    public static Typeface TYPEFACE_REGULAR;
    public static Typeface TYPEFACE_BOLD;

    public static void initialize(Context context) {
        if (TYPEFACE_BOLD == null) {
            TYPEFACE_BOLD = Typeface.createFromAsset(context.getAssets(), ASSET_PATH_BOLD);
        }
        if (TYPEFACE_REGULAR == null) {
            TYPEFACE_REGULAR = Typeface.createFromAsset(context.getAssets(), ASSET_PATH_REGUALR);
        }
        if (TYPEFACE_LIGHT == null) {
            TYPEFACE_LIGHT = Typeface.createFromAsset(context.getAssets(), ASSET_PATH_LIGHT);
        }
    }

    public static void applyFontToMenuItem(MenuItem mi, Typeface typeface) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new GTTextSpan("", typeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public static void applyTypeface(View view, Typeface typeface) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                applyTypeface(viewGroup.getChildAt(i), typeface);
            }
        } else if (view instanceof TextView) {
            ((TextView) view).setTypeface(typeface);
        }
    }

    public static SpannableString applyTypeface(CharSequence charSequence){
        SpannableString spanTitle = new SpannableString(charSequence);
        spanTitle.setSpan(new GTTextSpan("" , TypefaceManager.TYPEFACE_REGULAR), 0 , spanTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return spanTitle;
    }
}
