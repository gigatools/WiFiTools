package com.gigatools.wifianalyzer.widgets;

import android.support.annotation.IntDef;

import static com.gigatools.wifianalyzer.widgets.FontStyle.BOLD;
import static com.gigatools.wifianalyzer.widgets.FontStyle.LIGHT;
import static com.gigatools.wifianalyzer.widgets.FontStyle.REGULAR;

@IntDef({LIGHT, REGULAR, BOLD})
public @interface FontStyle {
    int LIGHT = 0;
    int REGULAR = 1;
    int BOLD = 2;
}