package com.gigatools.wifianalyzer.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.gigatools.wifianalyzer.R;

import static com.gigatools.wifianalyzer.widgets.TypefaceManager.TYPEFACE_BOLD;
import static com.gigatools.wifianalyzer.widgets.TypefaceManager.TYPEFACE_LIGHT;
import static com.gigatools.wifianalyzer.widgets.TypefaceManager.TYPEFACE_REGULAR;

public class GTTextView extends AppCompatTextView {
    @FontStyle
    private int fontStyle;

    public GTTextView(Context context) {
        super(context);
        init(context, null);
    }

    public GTTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GTTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setFontStyle(@FontStyle int fontStyle) {
        this.fontStyle = fontStyle;
        setTypeface(fontStyle == FontStyle.REGULAR ? TYPEFACE_REGULAR :
            (fontStyle == FontStyle.LIGHT ? TYPEFACE_LIGHT : TYPEFACE_BOLD));
    }

    private void init(Context context, AttributeSet attrs) {
        fontStyle = FontStyle.REGULAR;
        if (attrs == null)
            return;
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.GTTextView);
        try {
            fontStyle = array.getInt(R.styleable.GTTextView_typefaceStyle, FontStyle.REGULAR);
            setFontStyle(fontStyle);
        } finally {
            array.recycle();
        }
    }
}
