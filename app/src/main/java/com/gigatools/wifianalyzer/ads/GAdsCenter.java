package com.gigatools.wifianalyzer.ads;

import com.facebook.ads.AdSettings;

public class GAdsCenter {
    public static final String TAG = "AdsCenter";
    private static GAdsCenter instance;

    private GAdsCenter() {
    }

    public static GAdsCenter getInstance() {
        if (instance == null)
            instance = new GAdsCenter();
        return instance;
    }

    public GAdsCenter addAdmobTestDevice(String hash) {
        LibConfig.getInstance().listAdmobTestDevices.add(hash);
        return this;
    }

    public GAdsCenter addFANTestDevice(String hash) {
        LibConfig.getInstance().listFANTestDevices.add(hash);
        AdSettings.addTestDevice(hash);
        return this;
    }
}
