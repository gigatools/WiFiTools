package com.gigatools.wifianalyzer.ads;

import android.content.Context;
import android.os.Bundle;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.gigatools.wifianalyzer.analytics.AnalyticsManager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class GInterstitial {
    private static final String EVENT_AD_LOADED = "ad_interstitial_loaded";
    private static final String EVENT_AD_LOAD_FAILED = "ad_interstitial_load_failed";
    private static final String EVENT_AD_IMPRESSION = "ad_interstitial_impression";
    private static final String EVENT_AD_CLICKED = "ad_interstitial_clicked";
    private static final String AD_TYPE_ADMOB = "admob";
    private static final String AD_TYPE_FAN = "facebook";
    private static final String KEY_AD_TYPE = "ad_type";
    private static final String KEY_AD_ERROR_TYPE = "ad_error_type";
    private Context context;
    private AdConfig config;
    private InterstitialAd admobInterstitial;
    private com.facebook.ads.InterstitialAd fbInterstitial;
    private AdCallback adCallback;

    public GInterstitial(Context context, AdConfig config) {
        this.config = config;
        this.context = context;
    }

    private void loadAdmobInterstitial() {
        admobInterstitial = new InterstitialAd(context);
        admobInterstitial.setAdUnitId(config.admobID);
        AdRequest.Builder builder = new AdRequest.Builder();
        if (LibConfig.getInstance().listAdmobTestDevices != null) {
            for (String hash : LibConfig.getInstance().listAdmobTestDevices) {
                builder.addTestDevice(hash);
            }
        }
        AdRequest adRequest = builder.build();
        admobInterstitial.setAdListener(new AdListener() {

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_ADMOB);
                AnalyticsManager.logEvent(EVENT_AD_CLICKED, params);
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_ADMOB);
                AnalyticsManager.logEvent(EVENT_AD_IMPRESSION, params);
            }

            @Override
            public void onAdClosed() {
                if (adCallback != null) {
                    adCallback.doNext();
                }
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_ADMOB);
                AnalyticsManager.logEvent(EVENT_AD_LOADED, params);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_ADMOB);
                String errorType;
                switch (i) {
                    case AdRequest.ERROR_CODE_NO_FILL:
                        errorType = "ERROR_CODE_NO_FILL";
                        break;
                    case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                        errorType = "ERROR_CODE_INTERNAL_ERROR";
                        break;
                    case AdRequest.ERROR_CODE_NETWORK_ERROR:
                        errorType = "ERROR_CODE_NETWORK_ERROR";
                        break;
                    case AdRequest.ERROR_CODE_INVALID_REQUEST:
                        errorType = "ERROR_CODE_INVALID_REQUEST";
                        break;
                    default:
                        errorType = "ERROR_UNKNOW";
                        break;
                }
                params.putString(KEY_AD_ERROR_TYPE, errorType);
                AnalyticsManager.logEvent(EVENT_AD_LOAD_FAILED, params);
                loadFANInterstitial();
            }
        });
        admobInterstitial.loadAd(adRequest);
    }

    private void loadFANInterstitial() {
        fbInterstitial = new com.facebook.ads.InterstitialAd(context, config.fanID);
        fbInterstitial.setAdListener(new InterstitialAdListener() {
            @Override
            public void onLoggingImpression(Ad ad) {
                Bundle params = new Bundle();
                params.putString(AD_TYPE_FAN, AD_TYPE_FAN);
                AnalyticsManager.logEvent( EVENT_AD_IMPRESSION, params);
            }

            @Override
            public void onInterstitialDisplayed(Ad ad) {
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                if (adCallback != null) {
                    adCallback.doNext();
                }
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_FAN);
                String errorType;
                switch (adError.getErrorCode()) {
                    case AdError.NO_FILL_ERROR_CODE:
                        errorType = "ERROR_CODE_NO_FILL";
                        break;
                    case AdError.INTERNAL_ERROR_CODE:
                        errorType = "ERROR_CODE_INTERNAL_ERROR";
                        break;
                    case AdError.NETWORK_ERROR_CODE:
                        errorType = "ERROR_CODE_NETWORK_ERROR";
                        break;
                    case AdError.SERVER_ERROR_CODE:
                        errorType = "SERVER_ERROR_CODE";
                        break;
                    case AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE:
                        errorType = "LOAD_TOO_FREQUENTLY_ERROR_CODE";
                        break;
                    case AdError.CACHE_ERROR_CODE:
                        errorType = "CACHE_ERROR_CODE";
                        break;
                    case AdError.MEDIATION_ERROR_CODE:
                        errorType = "MEDIATION_ERROR_CODE";
                        break;
                    default:
                        errorType = "ERROR_UNKNOW";
                        break;
                }
                params.putString(KEY_AD_ERROR_TYPE, errorType);
                AnalyticsManager.logEvent( EVENT_AD_LOAD_FAILED, params);
                loadAdmobInterstitial();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_FAN);
                AnalyticsManager.logEvent( EVENT_AD_LOADED, params);
            }

            @Override
            public void onAdClicked(Ad ad) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_FAN);
                AnalyticsManager.logEvent( EVENT_AD_CLICKED, params);
            }
        });
        fbInterstitial.loadAd();
    }

    public void loadAd() {
        if (config.showAdmobFirst) {
            loadAdmobInterstitial();
        } else {
            loadFANInterstitial();
        }
    }

    private void showAdmobAd() {
        if (admobInterstitial != null && admobInterstitial.isLoaded()) {
            admobInterstitial.show();
            config.showAdmobFirst = !config.showAdmobFirst;
            loadAd();
        } else if (fbInterstitial != null && fbInterstitial.isAdLoaded()) {
            showFANAd();
        } else {
            if (adCallback != null) {
                adCallback.doNext();
            }
        }
    }

    private void showFANAd() {
        if (fbInterstitial != null && fbInterstitial.isAdLoaded()) {
            fbInterstitial.show();
            config.showAdmobFirst = !config.showAdmobFirst;
            loadAd();
        } else if (admobInterstitial != null && admobInterstitial.isLoaded()) {
            showAdmobAd();
        } else {
            if (adCallback != null) {
                adCallback.doNext();
            }
        }
    }

    public void showAd(AdCallback callback) {
        this.adCallback = callback;
        if (config.showAdmobFirst) {
            showAdmobAd();
        } else {
            showFANAd();
        }
    }

    public void onDestroy() {
        if (fbInterstitial != null)
            fbInterstitial.destroy();
    }
}
