package com.gigatools.wifianalyzer.ads;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.gigatools.wifianalyzer.analytics.AnalyticsManager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class GBanner {
    private static final long DELAY_TIME_RELOAD_AD = 15 * 1000;
    private static final int MAX_RETRY_COUNT = 5;
    private static final String EVENT_AD_LOADED = "ad_banner_loaded";
    private static final String EVENT_AD_LOAD_FAILED = "ad_banner_load_failed";
    private static final String EVENT_AD_IMPRESSION = "ad_banner_impression";
    private static final String EVENT_AD_CLICKED = "ad_banner_clicked";
    private static final String AD_TYPE_ADMOB = "admob";
    private static final String AD_TYPE_FAN = "facebook";
    private static final String KEY_AD_TYPE = "ad_type";
    private static final String KEY_AD_ERROR_TYPE = "ad_error_type";
    private AdView admobBanner;
    private com.facebook.ads.AdView fbBanner;
    private RelativeLayout container;
    private AdConfig adConfig;
    private Context context;
    private int retryCount = 0;

    public GBanner(Context context, AdConfig adConfig, RelativeLayout container) {
        this.context = context;
        this.adConfig = adConfig;
        this.container = container;
        container.setVisibility(View.GONE);
    }

    public void onResume() {
        if (admobBanner != null)
            admobBanner.resume();
    }

    public void onDestroy() {
        if (admobBanner != null)
            admobBanner.destroy();
        if (fbBanner != null)
            fbBanner.destroy();
    }

    public void onPause() {
        if (admobBanner != null)
            admobBanner.pause();
    }

    private void loadAndShowAdmob() {
        Log.v("WiFiAnalyzer", "Load ADMOB "+retryCount);
        if (retryCount > MAX_RETRY_COUNT) {
            return;
        }
        admobBanner = new AdView(context);
        admobBanner.setAdSize(AdSize.SMART_BANNER);
        admobBanner.setAdUnitId(adConfig.admobID);
        admobBanner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                container.setVisibility(View.VISIBLE);
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_ADMOB);
                AnalyticsManager.logEvent(EVENT_AD_IMPRESSION, params);
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
                //Need to track impression in onAdLoaded
            }

            @Override
            public void onAdOpened() {
                super.onAdClicked();
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_ADMOB);
                AnalyticsManager.logEvent(EVENT_AD_CLICKED, params);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_ADMOB);
                String errorType;
                switch (i) {
                    case AdRequest.ERROR_CODE_NO_FILL:
                        errorType = "ERROR_CODE_NO_FILL";
                        break;
                    case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                        errorType = "ERROR_CODE_INTERNAL_ERROR";
                        break;
                    case AdRequest.ERROR_CODE_NETWORK_ERROR:
                        errorType = "ERROR_CODE_NETWORK_ERROR";
                        break;
                    case AdRequest.ERROR_CODE_INVALID_REQUEST:
                        errorType = "ERROR_CODE_INVALID_REQUEST";
                        break;
                    default:
                        errorType = "ERROR_UNKNOW";
                        break;
                }
                params.putString(KEY_AD_ERROR_TYPE, errorType);
                AnalyticsManager.logEvent(EVENT_AD_LOAD_FAILED, params);
                container.setVisibility(View.GONE);
                retryCount++;
                if (retryCount == 1) {
                    loadAndShowFAN();
                } else {
                    container.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.v("WiFiAnalyzer", "Retry FAN Delayed "+retryCount);
                            loadAndShowFAN();
                        }
                    }, DELAY_TIME_RELOAD_AD);
                }
            }
        });
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        admobBanner.setLayoutParams(params);
        container.removeAllViews();
        container.addView(admobBanner);
        AdRequest.Builder builder = new AdRequest.Builder();
        if (LibConfig.getInstance().listAdmobTestDevices != null) {
            for (String hash : LibConfig.getInstance().listAdmobTestDevices) {
                builder.addTestDevice(hash);
            }
        }
        AdRequest adRequest = builder.build();
        admobBanner.loadAd(adRequest);
    }

    private void loadAndShowFAN() {
        Log.v("WiFiAnalyzer", "Load FAN "+retryCount);
        if (retryCount > MAX_RETRY_COUNT) {
            return;
        }
        fbBanner = new com.facebook.ads.AdView(context, adConfig.fanID, com.facebook.ads.AdSize.BANNER_HEIGHT_50);
        fbBanner.setAdListener(new com.facebook.ads.AdListener() {
            @Override
            public void onLoggingImpression(Ad ad) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_FAN);
                AnalyticsManager.logEvent(EVENT_AD_IMPRESSION, params);
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_FAN);
                String errorType;
                switch (adError.getErrorCode()) {
                    case AdError.NO_FILL_ERROR_CODE:
                        errorType = "ERROR_CODE_NO_FILL";
                        break;
                    case AdError.INTERNAL_ERROR_CODE:
                        errorType = "ERROR_CODE_INTERNAL_ERROR";
                        break;
                    case AdError.NETWORK_ERROR_CODE:
                        errorType = "ERROR_CODE_NETWORK_ERROR";
                        break;
                    case AdError.SERVER_ERROR_CODE:
                        errorType = "SERVER_ERROR_CODE";
                        break;
                    case AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE:
                        errorType = "LOAD_TOO_FREQUENTLY_ERROR_CODE";
                        break;
                    case AdError.CACHE_ERROR_CODE:
                        errorType = "CACHE_ERROR_CODE";
                        break;
                    case AdError.MEDIATION_ERROR_CODE:
                        errorType = "MEDIATION_ERROR_CODE";
                        break;
                    default:
                        errorType = "ERROR_UNKNOW";
                        break;
                }
                params.putString(KEY_AD_ERROR_TYPE, errorType);
                AnalyticsManager.logEvent(EVENT_AD_LOAD_FAILED, params);
                container.setVisibility(View.GONE);
                retryCount++;
                if (retryCount == 1) {
                    loadAndShowAdmob();
                } else {
                    container.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.v("WiFiAnalyzer", "Retry ADMOB Delayed "+retryCount);
                            loadAndShowAdmob();
                        }
                    }, DELAY_TIME_RELOAD_AD);
                }
            }

            @Override
            public void onAdLoaded(Ad ad) {
                container.setVisibility(View.VISIBLE);
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_FAN);
                AnalyticsManager.logEvent(EVENT_AD_LOADED, params);
            }

            @Override
            public void onAdClicked(Ad ad) {
                Bundle params = new Bundle();
                params.putString(KEY_AD_TYPE, AD_TYPE_FAN);
                AnalyticsManager.logEvent(EVENT_AD_CLICKED, params);
            }
        });
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        fbBanner.setLayoutParams(params);
        fbBanner.loadAd();
        container.removeAllViews();
        container.addView(fbBanner);
    }

    public void loadAd() {
        retryCount = 0;
        if (adConfig.showAdmobFirst) {
            loadAndShowAdmob();
        } else {
            loadAndShowFAN();
        }
    }
}
