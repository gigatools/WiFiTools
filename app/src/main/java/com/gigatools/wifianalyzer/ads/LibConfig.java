package com.gigatools.wifianalyzer.ads;

import java.util.ArrayList;
import java.util.List;

class LibConfig {
    private static LibConfig instance;

    private LibConfig() {
        listAdmobTestDevices = new ArrayList<>();
        listFANTestDevices = new ArrayList<>();
    }

    public static synchronized LibConfig getInstance() {
        if (instance == null) {
            instance = new LibConfig();
        }
        return instance;
    }

    public List<String> listAdmobTestDevices;
    public List<String> listFANTestDevices;
}
