package com.gigatools.wifianalyzer.ads;

public class AdConfig {
    public String adName;
    public String admobID;
    public String fanID;
    public boolean showAdmobFirst;

    public AdConfig() {
    }

    public AdConfig(String adName, String admobID, String fanID, boolean admobFirst) {
        this.admobID = admobID;
        this.fanID = fanID;
        this.showAdmobFirst = admobFirst;
        this.adName = adName;
    }

    public AdConfig(String admobID, String fanID, boolean showAdmobFirst) {
        this.admobID = admobID;
        this.fanID = fanID;
        this.showAdmobFirst = showAdmobFirst;
    }
}
